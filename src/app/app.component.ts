import { Component, OnInit } from '@angular/core';
import { interval, of, from, BehaviorSubject, combineLatest } from 'rxjs';
import { take, map, tap, filter, find, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  template: `
  <h1>{{ message }}</h1>
  <span>{{count$ | async }}</span>
  <button [disabled]="(count$ | async) === 0" (click)="changeCount('decrement')">decrement</button>
  <button (click)="changeCount('increment')">increment</button>
  <button *ngIf="(count$ | async)! > 0" (click)="changeCount('reset')">reset to 0</button>
  <button *ngIf="(count$ | async)! > 1" (click)="changeCount('double')">double</button>
  `,
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  message: string = '';
  name = 'App root';

  //counter
  start$ = of(0);
  actionSubject = new BehaviorSubject<number>(0);
  action$ = this.actionSubject.asObservable();
  count$ = combineLatest([this.start$, this.action$]).pipe(
    map(([startingValue, action]) => {
      return startingValue + action;
    })
  );

  changeCount(action: string) {
    const currentValue = this.actionSubject.getValue();
    if (action === "decrement") {
      this.actionSubject.next(currentValue - 1);
    }
    if (action === "increment") {
      this.actionSubject.next(currentValue + 1);
    }
    if (action === "double") {
      this.actionSubject.next(currentValue + currentValue);
    }
    if (action === "reset") {
      this.actionSubject.next(currentValue - currentValue);
    }
  }

  ngOnInit() {

    this.message = 'App component';

    //of operator (Creation operator)

    const arr = [1, 2, 3];
    const arr$ = of(arr);

    arr$.subscribe((x) => console.log(`of operator: `, x));

    //of operator with next (Creation operator)

    const array = [10, 20, 30]
    const array$ = of(array);

    array$.subscribe({
      next: (x) => console.log("of operator with 'Next':", x),
      error: (e) => console.error(e),
      complete: () => console.info('complete')
    })

    //from operator (Creation operator)

    const arrayOfNumbers = [100, 200, 300]
    const fromNumbers$ = from(arrayOfNumbers);
    fromNumbers$.subscribe((x) => console.log(`single number: `, x));

    //from operator with a string (Creation operator)

    const string = "hello world"
    const fromString$ = from(string);
    fromString$.subscribe((x) => console.log(`string: `, x));

    //map operator (Transformation operator)
    //pipe operator must be used for transformation operators

    const hello = "hello";
    const fromHello$ = of(hello);
    fromHello$
      .pipe(map((x) => x + " world"))
      .subscribe((x) => console.log(`result of map: `, x));


    //map operator with from operator (Transformation operator)

    const addOne = [500, 600, 700];
    const addOne$ = from(addOne);
    addOne$
      .pipe(map((x) => x + 1))
      .subscribe((x) => console.log(`result of add one to array of numbers: `, x));

    //tap operator (Transformation operator)

    const tapExample = "old value";
    const tapExample$ = of(tapExample);
    tapExample$
      .pipe(tap((x) => console.log("tap received: ", x)),
        (map((x) => x + " + new value")))
      .subscribe((x) => console.log("tap emitted: ", x));

    //take operator (Filtering operator)
    //pipe operator must be used for filtering operators

    const numbers = interval(1000);
    const takeThree$ = numbers.pipe(take(3));

    takeThree$.subscribe(x => console.log("Subscriber: " + x));

    //filter operator (Filtering operator)

    const filterNumbers = [1, 2, 3, 4, 5];
    const filterNumbers$ = of(filterNumbers);
    filterNumbers$.pipe(
      filter(x => x.length > 0),
    ).subscribe(x => {
      console.log("log array only if major than 0: ", x);
    })

    //find operator (Filtering operator)

    const filterNumber$ = from(filterNumbers);
    filterNumber$.pipe(
      find(x => x > 4)
    ).subscribe(x => {
      console.log("log number only if major than 4: ", x);
    })

    //startWith operator (Creation operator)

    const startWithFirst$ = of(filterNumbers);
    startWithFirst$.pipe(
      startWith([0])
    ).subscribe(x => {
      console.log("log the first number of the aray: ", x);
    })

  }
}

